# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04 LTS - Xenial Xerus - 64-bit - Fuga Cloud Based Image"

az_list = ["eu-west-1a"]

# masters
flavor_k8s_master = "876e8582-c9ed-444e-b157-c7d1604398e6"

# networking
external_net = "fa780b35-5449-4b2f-a17a-37dd0b12a351"
floatingip_pool = "external"
