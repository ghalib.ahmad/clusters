# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04"

# masters
flavor_k8s_master = "2f5e4d34-3784-4ea6-be33-8501b056ac08" # s1-8
number_of_k8s_masters = 0
number_of_k8s_masters_no_floating_ip = 1

# networking
network_name = "Ext-Net"
external_net = "6c928965-47ea-463f-acc8-6d4a152e9745"
floatingip_pool = "Ext-Net"
dns_nameservers = []
use_neutron = 0
