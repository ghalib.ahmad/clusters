# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04"

# masters
flavor_k8s_master = "3be7c73a-735a-4ee1-b8d4-83feb080109d" # b2-7
number_of_k8s_masters = 0
number_of_k8s_masters_no_floating_ip = 1

# networking
network_name = "Ext-Net"
external_net = "ed0ab0c6-93ee-44f8-870b-d103065b1b34"
floatingip_pool = "Ext-Net"
dns_nameservers = []
use_neutron = 0
