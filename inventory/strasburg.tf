# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04"

# masters
flavor_k8s_master = "ca4c4774-a9bf-4739-b424-6c6f54ced3bf" # s1-8
number_of_k8s_masters = 0
number_of_k8s_masters_no_floating_ip = 1

# networking
network_name = "Ext-Net"
external_net = "581fad02-158d-4dc6-81f0-c1ec2794bbec"
floatingip_pool = "Ext-Net"
dns_nameservers = []
use_neutron = 0
