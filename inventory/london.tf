# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04 Xenial Xerus"

# masters
flavor_k8s_master = "18b9b1a7-d550-4e24-a2ee-8812cdb08e3d"

# networking
external_net = "9e031103-e161-4e71-8740-24cd16beb239"
floatingip_pool = "ext-net"
