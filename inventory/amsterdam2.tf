# image to use for bastion, masters, standalone etcd instances, and nodes
image = "GNU/Linux Ubuntu Server 16.04 Xenial Xerus x64"

# masters
flavor_k8s_master = "340"

# networking
external_net = "1ed015ee-e0ef-4736-b499-c708f00335cd"
floatingip_pool = "PublicNetwork"
