# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04 Xenial Xerus"

# masters
flavor_k8s_master = "5682a941-201e-409f-ae28-e943a43c74ad" # 1C-4GB-20GB

# networking
external_net = "2aec7a99-3783-4e2a-bd2b-bbe4fef97d1c"
floatingip_pool = "ext-net"
