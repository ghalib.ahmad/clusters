# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04 LTS x64"

az_list = ["AZ2"]

# masters
flavor_k8s_master = "114" # S.mem+

# networking
external_net = "585ec5ec-5993-4042-93b9-264b0d82ac8e"
floatingip_pool = "shared-public-IPv4"
