# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04"

# masters
flavor_k8s_master = "ea5c6bc6-068f-479d-8193-0f1170e41fcb" # s1-8
number_of_k8s_masters = 0
number_of_k8s_masters_no_floating_ip = 1

# networking
network_name = "Ext-Net"
external_net = "b007ac8f-373e-4eaa-9b6b-7f8e49ba3ff9"
floatingip_pool = "Ext-Net"
dns_nameservers = []
use_neutron = 0
