# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu 16.04"

az_list = ["ca-ymq-2"]

# masters
flavor_k8s_master = "52a01f6b-e660-48b5-8c06-5fb2a0fab0ec"

# networking
external_net = "6d6357ac-0f70-4afa-8bd7-c274cc4ea235"
floatingip_pool = "public"
