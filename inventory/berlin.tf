# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu-16.04_x86_64"

# masters
flavor_k8s_master = "80217271-1c89-4b71-9fe2-22e50379befa" # m1.medium

# networking
external_net = "fd998c88-046f-423b-8cb2-1afe1dae0b5e"
floatingip_pool = "public_external"
