#!/usr/bin/env python
import requests
import re
import argparse
import os
import logging
import gitlab
from pprint import pprint

logging.basicConfig(level=logging.NOTSET)

parser = argparse.ArgumentParser()
parser.add_argument('--token', help='GitLab Token',
                    default=os.getenv('GL_TOKEN'))
parser.add_argument('--kube-token', help='Kubernetes Token',
                    default=os.getenv('KUBE_TOKEN', ''))
parser.add_argument('--kube-ca-cert', help='Kubernetes CA Certificate',
                    default=os.getenv('KUBE_CA_CERT', ''))
parser.add_argument('--kube-apiserver',
                    help='Kubernetes API server', required=True)
parser.add_argument('--kube-namespace',
                    help='Kubernetes app namespace', default='app')
parser.add_argument('--gitlab-api', default=os.getenv('GL_API', 'https://gitlab.com/'),
                    help='GitLab API endpoint, defaults to gitlab.com')
parser.add_argument('--project', help='GitLab project path', required=True)
parser.add_argument('--cluster', help='Cluster name', required=True)

args = parser.parse_args()


def auth():
    gl = gitlab.Gitlab(args.gitlab_api, private_token=args.token)
    gl.auth()
    return gl


def get_vars(name, api, token, ca_cert, namespace):
    return {
        'KUBE_URL_' + name: api,
        'KUBE_TOKEN_' + name: token,
        'KUBE_CA_PEM_' + name: ca_cert,
        'KUBE_NAMESPACE_' + name: namespace
    }


# Update/Insert a variable
def upsert(key, value):
    project = auth().projects.get(args.project)
    try:
        # Delete previous variable
        project.variables.get(key).delete()
    except gitlab.exceptions.GitlabGetError:
        # Ignore delete error if variable doesn't exist
        pass
    project.variables.create({'key': key, 'value': value})


def set_cluster(name, api, token, ca_cert):
    for key, val in get_vars(name, api, token, ca_cert, namespace='app').items():
        upsert(key, val)


if __name__ == "__main__":
    set_cluster(args.cluster, args.kube_apiserver,
                args.kube_token, args.kube_ca_cert)
