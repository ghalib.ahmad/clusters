terraform {
  backend "gcs" {
    bucket  = "terraform-multicloud-openstack-k8s"
    prefix  = "terraform/state"
  }
}
