import yaml
import json

with open("clouds.yaml", 'r') as f:
    try:
        clouds = yaml.load(f)
    except yaml.YAMLError as e:
        print(e)


def get_data():
    for name, config in clouds['clouds'].items():
        if '_provider' in config:
            yield {'cluster': name,
            'provider': config['_provider'],
            'latlng': config['_latlng'],
            'cluster_status': 'unknown',
            'app_status': 'unknown',
            }


print(json.dumps(list(get_data())))
